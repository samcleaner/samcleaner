     What's New                                       com.samsung.android.app.social                                  1
     Google Play Services Car Policy                  com.google.android.gms.policy_sidecar_aps                       1
     Google AR services                               com.google.ar.core                                              1
     VR setup wizard                                  com.samsung.android.app.vrsetupwizardstub                       1
     Bixby voice wake-up                              com.samsung.android.bixby.wakeup                                1
     Game Tuner                                       com.samsung.android.gametuner.thin                              1
     Gear VR shell                                    com.samsung.android.hmt.vrshell                                 1
     Samsung Keyboard                                 com.samsung.android.honeyboard                                  1
     Smart Switch Assinstant                          com.samsung.android.smartswitchassistant                        1
     Compatibility Test Suite shim                    com.android.cts.ctsshim                                         1
     Compatibility Test Suite                         com.android.cts.priv.ctsshim                                    1
     Samsung+ (Samsung Members)                       com.samsung.oh                                                  1
     Personalization                                  com.sec.android.app.personalization                             1
     Samsung Music                                    com.sec.android.app.music                                       1
     Samsung Calculator                               com.sec.android.app.popupcalculator                             1
     Secure Element Application                       com.android.se                                                  1
     Config APK                                       android.autoinstalls.config.samsung                             1
     Beacon Service                                   com.sec.bcservice                                               1
     SIM Toolkit (STK) instructions                   com.android.stk                                                 1
     SIM Toolkit (STK) instructions v2                com.android.stk2                                                1
     Partner bookmarks                                com.android.providers.partnerbookmarks                          1
     Weather widget                                   com.sec.android.widgetapp.at.hero.accuweather                   1
     Bundleware installer                             com.LogiaGroup.LogiaDeck                                        1
     Beaming/NFC Service                              com.mobeam.barcodeService                                       1
     Samsung Cloud Backup                             com.android.sharedstoragebackup                                 1
     Google Services Framework login                  com.google.android.gsf.login                                    1
     Chrome Customizations                            com.sec.android.app.chromecustomizations                        1
     Google Play Movies & TV                          com.google.android.videos                                       1
     Voice Access                                     com.google.android.apps.accessibility.voiceaccess               1
     Google Play Books                                com.google.android.apps.books                                   1
     Google Docs                                      com.google.android.apps.docs                                    1
     Google Docs                                      com.google.android.apps.docs.editors.docs                       1
     Google News                                      com.google.android.apps.magazines                               1
     Google Messages                                  com.google.android.apps.messaging                               1
     Google+ for G Suite                              com.google.android.apps.plus                                    1
     Google Duo                                       com.google.android.apps.tachyon                                 1
     Google Pay                                       com.google.android.apps.walletnfcrel                            1
     Google Play Music                                com.google.android.music                                        1
     Google Play Games                                com.google.android.play.games                                   1
     Google Street View                               com.google.android.street                                       1
     Google Hangouts                                  com.google.android.talk                                         1
     Hancom Office Widget                             com.hancom.androidpc.appwidget                                  1
     Hancom Updated                                   com.hancom.androidpc.hanupdater                                 1
     Hancom Launcher                                  com.hancom.androidpc.launcher.shared                            1
     Hancom Viewer                                    com.hancom.androidpc.viewer.launcher                            1
     Hancom Cell Viewer                               com.hancom.office.hcell.viewer.hcell_viewer_apk                 1
     Hancom Show Viewer                               com.hancom.office.hshow.viewer.hshow_viewer_apk                 1
     Hancom Word Viewer                               com.hancom.office.hword.viewer.hword_apk                        1
     Hancom Editor Downloader                         com.hancom.office.editor.hidden                                 1
     Daydream Screensaver Customization               com.samsung.daydream.customization                              1
     Network Service Discovery (NSDSWebApp)           com.sec.vsim.ericssonnsds.webapp                                1
     Digital Wellbeing                                com.samsung.android.forest                                      1
     SamsungOdaService                                com.samsung.oda.service                                         1
     SmartThings Framework                            com.samsung.android.service.stplatform                          1
     Separated Apps                                   com.samsung.android.appseparation                               1
     Private Share                                    com.samsung.android.privateshare                                1
     Continuity Service                               com.samsung.android.mcfds                                       1
     TalkBack                                         com.samsung.android.accessibility.talkback                      1
