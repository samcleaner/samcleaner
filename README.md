# SamCleaner - Sam cleaner of android garbage

![screenshot](assets/screenshot.png "SamCleaner screenshot")

SamCleaner is a python+gtk application that helps manage packages in android
devices via `adb`. This is achieved via two methods, one is by listing all
available packages on the device with the possibility of enabling/disabling
individual packages by name, while the second method is by using a blacklist
file (in JSON format) thus allowing the user to automate the procedure.


## Features

Support for adb commands
  - [x] version
  - [x] devices
  - [x] list packages
  - [x] enable
  - [x] disable
  - [x] uninstall
  - [ ] permissions
  - [ ] grant
  - [ ] revoke
  - [ ] dump
  - [x] adb via ssh
  - [x] multiple connected devices

Packages
  - [x] list installed packages
  - [x] individual enable/disable option
  - [x] blacklist (JSON format)
  - [x] apply mass uninstall + disable
  - [x] start/stop procedure
  - [x] sort packages by column
  - [x] search packages by ID and name
  - [x] clever blacklist package selection

Blacklist
  - [x] system + GPS/NFC services
  - [x] bundleware
  - [x] spyware
  - [x] ANT remote services
  - [x] facebook
  - [x] google
  - [x] microsoft
  - [x] hancom
  - [x] samsung

General
  - [x] open source
  - [x] GTK interface
  - [x] multi-threaded
  - [x] configurable parameters
  - [x] no internet connection
  - [x] GDPR compliant
  - [x] learn python in 1 week :yum:


## Requirements

- GTK+3
- Python 3
- Gobject-introspection
- adb

You do not need the whole android development studio, you only need the
[platform tools package](https://developer.android.com/studio/releases/platform-tools)
which contains a fully working `adb`.


## A word of warning

The included blacklist is rather extreme, it is a personal preference and
definitely not suitable for general use, so it **WILL BREAK YOUR DEVICE**.
You have been warned. Below is a partial list of modifications:

- remove GPS
- remove NFC
- remove push messages
- remove ANT
- remove all apps by facebook
- remove all apps by microsoft
- remove most apps by samsung
- remove most apps by google
- disable the google play store

The user should modify the included blacklist to suit their individual needs.


## Usage

Simply run the application via the `samcleaner.py` file, which is a python executable script. The main application window is split into two sections, the left shows you the loaded blacklist and the right shows you the installed packages of the connected device.

Please edit the preferences and set the `adb` full path and filename and optionally add an ssh command if you are connecting to a remote system. Once `adb` is setup, the right section should show the installed packages and the *Apply* button will now be clickable. The *Find* button (or CTRL-F) opens a search entry field that searches by ID or package name.

When the *Apply* button is clicked, SamCleaner will start the clean-up procedure and loop through the blacklisted packages and perform an action (uninstall or disable). The procedure can be stopped at any time by clicking on the *Stop* button.

