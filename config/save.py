"""
SamCleaner config save functions

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import config

def save():
	"""Save configuration to samcleaner.conf file"""

	try:

		with open("samcleaner.conf", "w") as configfile:
			config.cfg.write(configfile)

	except:

		logging.error("config save(): failed to save the configuration file: samcleaner.conf")
		quit()

