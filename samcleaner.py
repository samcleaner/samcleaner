#!/usr/bin/env python3
"""
SamCleaner main application script

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk
import os, sys, json, re, logging, signal, threading, config, adb, apklist

class SamCleaner:
	"""SamCleaner main application

	Stracture and logic:

	__init__(), main function that performs basic preparations:
		- loads the config file
		- sets signal handlers
		- performs some basic file checks
		- loads the APK lists via the apklist module
		- loads the glade XML interface
		- sets references to UI elements
		- opens the main application window

	Once the main application window is shown, the show() signal runs:

	on_mainAppWindow_show(), the show signal function executes a separate
	thread and calls the statusOfAdb() function:

	statusOfAdb(), makes sure adb is properly installed and loads devices:
		- runs 'adb version'
		- runs 'adb devices -l'
		- populates the connected devices drop-down menu

	Once the connected device drop-down menu is updated/changed, the
	changed() signal runs:

	on_deviceSelector_changed(), makes a few UI updates and loads available
	packages directly from the selected device:
		- runs 'adb shell cmd package list packages'
		- finds package names by scanning the pre-loaded APK lists
		- populates the tree/cell view with package info
	"""

	#
	# ADB status
	#
	def statusOfAdb(self):
		"""This function executes 'adb version'. If successful, the
		version number is displayed in the main window status bar. If
		unsuccessful, the status is set to 'error' and an infobar
		warning is shown to the user."""

		logging.info("statusOfAdb: execute adb version")

		# Get selected device ID
		currentDeviceID = self.deviceSelector.get_active_id()

		# Disable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, False)

		# Remove all devices from device selector combobox
		self.deviceSelector.remove_all()

		# Update runtime parameter
		config.runtime['samcleaner']['currentDeviceID'] = ""

		# ADB version
		adbVersion = adb.getVersion()

		if adbVersion=="":

			# Error
			currentStatus = "error"

			# Set status
			config.setStatus(currentStatus)

			# Set status image based on adb status
			self.adbImageStatus.set_from_icon_name("gtk-no", Gtk.IconSize.LARGE_TOOLBAR)
			self.adbImageStatus.set_tooltip_text("Error while running adb. The adb executable was not found or adb is not properly setup.")

			# Show infobar
			GLib.idle_add(self.showInfoBar, "warning", "The adb executable was not found!")
			return

		# OK
		currentStatus = "ok"

		# Set status
		config.setStatus(currentStatus)

		# Set status image based on adb status
		self.adbImageStatus.set_from_icon_name("gtk-yes", Gtk.IconSize.LARGE_TOOLBAR)
		self.adbImageStatus.set_tooltip_text("adb version: "+adbVersion)

		# Hide infobar
		GLib.idle_add(self.on_infoBar_response, self)

		# ADB devices
		adbDevices = adb.getDevices()

		if len(adbDevices)==0:

			# Show infobar
			GLib.idle_add(self.showInfoBar, "warning", "No devices found!")
			return

		# Populate device selector combobox
		for i in range(len(adbDevices)):

			# Add to selector combobox
			self.deviceSelector.append(adbDevices[i][0], adbDevices[i][1])

		if currentDeviceID==None:

			# No previous selection, select the first device by default
			currentDeviceID = adbDevices[0][0]

		else:

			# Found flag
			foundID = False

			# Loop devices and verify that the selected device ID exists
			for i in range(len(adbDevices)):

				if currentDeviceID==adbDevices[i][0]:
					foundID = True
					break

			if foundID==False:

				# Overwrite selection with a valid ID
				currentDeviceID = adbDevices[0][0]

		# Select by device ID
		self.deviceSelector.set_active_id(currentDeviceID)

	#
	# Toggle (Enable/disable) package
	#
	def toggleAdbPackage(self, childIter):
		"""This function executes 'adb enable' or 'adb disable' based
		on the toggle status of the selected package.
		"""

		logging.info("togglePackage: execute adb toggle")

		if self.adbPackageStore.get_value(childIter, 4)==True:

			# Enable package
			adbRc = adb.packageEnable(self.adbPackageStore.get_value(childIter, 1))

		else:

			# Hardcoded check for Payment Services, if disabled or removed the device will automatically lock itself
			if self.adbPackageStore.get_value(childIter, 1)=="com.samsung.android.kgclient":

				# Force error status
				adbRc = False

				# Show infobar
				GLib.idle_add(self.showInfoBar, "error", "The Payment Services package should not be disabled!")

			else:

				# Disable package
				adbRc = adb.packageDisable(self.adbPackageStore.get_value(childIter, 1))

		# Enable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, True)

		# Enable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, True)

		# Stop spinner
		GLib.idle_add(self.phoneSpinnerStatus.stop)

		if adbRc==True:

			self.phoneImageStatus.set_from_icon_name("gtk-yes", Gtk.IconSize.LARGE_TOOLBAR)
			self.phoneImageStatus.set_tooltip_text("Package toggled")

		else:

			# Toggle checkbox
			self.adbPackageStore.set_value(childIter, 4, not self.adbPackageStore.get_value(childIter, 4))

			self.phoneImageStatus.set_from_icon_name("gtk-no", Gtk.IconSize.LARGE_TOOLBAR)
			self.phoneImageStatus.set_tooltip_text("Could not enable/disable package")

		# Show phone status image
		GLib.idle_add(self.phoneImageStatus.set_visible, True)

	#
	# ADB sam garbage cleaner
	#
	def adbSamGarbageCleaner(self):
		"""This function loops over the blacklist packages and calls
		packageUninstall() on each one. Since this runs as a separate
		thread, it also listens for a stop thread event.
		"""

		logging.info("adbSamGarbageCleaner: execute adb package uninstall/disable procedure")

		# Disable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, False)

		# Disable the apply button
		GLib.idle_add(self.garbageCleanerButton.set_sensitive, False)

		# Disable the test button
		GLib.idle_add(self.garbageTestButton.set_sensitive, False)

		# Disable the refresh menu button
		GLib.idle_add(self.refreshButton.set_sensitive, False)

		# Disable the preferences menu button
		GLib.idle_add(self.preferencesButton.set_sensitive, False)

		# Disable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, False)

		# Get the selector of the view
		viewSelector = self.adbBlacklistView.get_selection()

		# Get iterator from store
		treeIter = self.adbBlacklistStore.get_iter_first()

		# Loop over groups
		while treeIter is not None:

			# A group must have packages as children
			if self.adbBlacklistStore.iter_has_child(treeIter):

				# Iterator over packages
				childIter = self.adbBlacklistStore.iter_children(treeIter)

				# Loop over packages
				while childIter is not None:

					# Select current package within the view
					GLib.idle_add(viewSelector.select_iter, childIter)

					# Scroll to selection
					GLib.idle_add(self.adbBlacklistView.scroll_to_cell, self.adbBlacklistView.get_model().get_path(childIter))

					# Loop packages
					for rowPackage in self.adbPackageStore:

						# Find in packages and perform action
						if rowPackage[1]==self.adbBlacklistStore[childIter][1]:

							# Blacklist action
							if self.adbBlacklistStore[childIter][2]=="disable":

								# Disable package
								adb.packageDisable(self.adbBlacklistStore[childIter][1])

							elif self.adbBlacklistStore[childIter][2]=="uninstall":

								# Uninstall package
								adb.packageUninstall(self.adbBlacklistStore[childIter][1])

							# Package found, stop loop
							break

					# Stop or not?
					if self.stopThreadEvent.is_set():
						break

					# Move to next package
					childIter = self.adbBlacklistStore.iter_next(childIter)

			# Stop or not?
			if self.stopThreadEvent.is_set():
				break

			# Move to next group
			treeIter = self.adbBlacklistStore.iter_next(treeIter)

		# Disable the stop button
		GLib.idle_add(self.garbageStopButton.set_sensitive, False)

		# Enable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, True)

		# Disable the preferences menu button
		GLib.idle_add(self.preferencesButton.set_sensitive, True)

		# Disable the refresh menu button
		GLib.idle_add(self.refreshButton.set_sensitive, True)

		# Enable the test button
		GLib.idle_add(self.garbageTestButton.set_sensitive, True)

		# Enable the apply button
		GLib.idle_add(self.garbageCleanerButton.set_sensitive, True)

		# Enable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, True)

		# Unset the stop event
		if self.stopThreadEvent.is_set():
			self.stopThreadEvent.clear()

	#
	# ADB sam garbage tester
	#
	def adbSamGarbageTester(self):
		"""This function loops over the blacklist packages and updates
		the testDialog view with the package name and action. Since this
		runs as a separate thread, it also listens for a stop thread event.
		"""

		logging.info("adbSamGarbageTester: execute adb package test procedure")

		# Disable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, False)

		# Disable the apply button
		GLib.idle_add(self.garbageCleanerButton.set_sensitive, False)

		# Disable the test button
		GLib.idle_add(self.garbageTestButton.set_sensitive, False)

		# Disable the refresh menu button
		GLib.idle_add(self.refreshButton.set_sensitive, False)

		# Disable the preferences menu button
		GLib.idle_add(self.preferencesButton.set_sensitive, False)

		# Disable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, False)

		# Get the selector of the view
		viewSelector = self.adbBlacklistView.get_selection()

		# Get iterator from store
		treeIter = self.adbBlacklistStore.get_iter_first()

		# Loop over groups
		while treeIter is not None:

			# A group must have packages as children
			if self.adbBlacklistStore.iter_has_child(treeIter):

				# Iterator over packages
				childIter = self.adbBlacklistStore.iter_children(treeIter)

				# Loop over packages
				while childIter is not None:

					# Select current package within the view
					GLib.idle_add(viewSelector.select_iter, childIter)

					# Scroll to selection
					GLib.idle_add(self.adbBlacklistView.scroll_to_cell, self.adbBlacklistView.get_model().get_path(childIter))

					# Loop packages
					for rowPackage in self.adbPackageStore:

						# Find in packages and perform action
						if rowPackage[1]==self.adbBlacklistStore[childIter][1]:

							# Blacklist action
							if self.adbBlacklistStore[childIter][2]=="disable":

								# Disable package
								GLib.idle_add(self.testDialogResultsViewBuffer.insert_at_cursor, "[DISABLE] " + self.adbBlacklistStore[childIter][1] + "\n")

							elif self.adbBlacklistStore[childIter][2]=="uninstall":

								# Uninstall package
								GLib.idle_add(self.testDialogResultsViewBuffer.insert_at_cursor, "[UNINSTALL] " + self.adbBlacklistStore[childIter][1] + "\n")

							# Package found, stop loop
							break

					# Stop or not?
					if self.stopThreadEvent.is_set():
						break

					# Move to next package
					childIter = self.adbBlacklistStore.iter_next(childIter)

			# Stop or not?
			if self.stopThreadEvent.is_set():
				break

			# Move to next group
			treeIter = self.adbBlacklistStore.iter_next(treeIter)

		# Disable the stop button
		GLib.idle_add(self.garbageStopButton.set_sensitive, False)

		# Enable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, True)

		# Disable the preferences menu button
		GLib.idle_add(self.preferencesButton.set_sensitive, True)

		# Disable the refresh menu button
		GLib.idle_add(self.refreshButton.set_sensitive, True)

		# Enable the test button
		GLib.idle_add(self.garbageTestButton.set_sensitive, True)

		# Enable the apply button
		GLib.idle_add(self.garbageCleanerButton.set_sensitive, True)

		# Enable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, True)

		# Unset the stop event
		if self.stopThreadEvent.is_set():
			self.stopThreadEvent.clear()

	#
	# InfoBar shotcut function
	#
	def showInfoBar(self, priority, text):
		"""This function displays an infobar. It takes a string as a
		priority and a string of text as the message. The priority
		is used to alter the Gtk.MessageType of the infobar and to
		alter the image icon accordingly. The priority parameter
		can be one of: info, warning, question, error, other."""

		logging.info("showInfoBar: show infobar")

		# Get object references
		ib = self.builder.get_object("infoBar")
		ibImage = self.builder.get_object("infoBarImage")
		ibLabel = self.builder.get_object("infoBarLabel")

		# Set message type (info, warning, question, error, other)
		if priority=="info":

			ibImage.set_from_icon_name("gtk-dialog-info", Gtk.IconSize.LARGE_TOOLBAR)
			ib.props.message_type = Gtk.MessageType.INFO

		elif priority=="warning":

			ibImage.set_from_icon_name("gtk-dialog-warning", Gtk.IconSize.LARGE_TOOLBAR)
			ib.props.message_type = Gtk.MessageType.WARNING

		elif priority=="question":

			ibImage.set_from_icon_name("gtk-dialog-question", Gtk.IconSize.LARGE_TOOLBAR)
			ib.props.message_type = Gtk.MessageType.QUESTION

		elif priority=="error":

			ibImage.set_from_icon_name("gtk-dialog-error", Gtk.IconSize.LARGE_TOOLBAR)
			ib.props.message_type = Gtk.MessageType.ERROR

		else:

			ibImage.set_from_icon_name("gtk-help", Gtk.IconSize.LARGE_TOOLBAR)
			ib.props.message_type = Gtk.MessageType.OTHER

		# Set text
		ibLabel.set_text(text)

		# Show infobar
		ib.show()

	# Infobar signal
	def on_infoBar_response(self, object, data=None):

		logging.info("signal:infoBar:response: X hides the infoBar")

		# Get object references
		ib = self.builder.get_object("infoBar")

		# Hide infobar
		ib.hide()

	#
	# Preferences dialog
	#
	def on_preferencesDialog_show(self, object, data=None):

		logging.info("signal:preferencesDialog:show: show preferences dialog")

		# Get entry pointers
		entryAdb = self.builder.get_object("preferencesDialogEntryAdb")
		entrySsh = self.builder.get_object("preferencesDialogEntrySsh")
		buttonSave = self.builder.get_object("preferencesDialogButtonSave")
		buttonCancel = self.builder.get_object("preferencesDialogButtonCancel")

		# Set contents based on config
		entryAdb.set_text(config.cfg['samcleaner']['adb'])
		entrySsh.set_text(config.cfg['samcleaner']['ssh'])

		# Make sure the Save/Cancel buttons are visible
		buttonSave.show()
		buttonCancel.show()

	def on_preferencesDialogButtonSave_clicked(self, object, data=None):

		logging.info("signal:preferencesDialogButtonSave:clicked: save preferences button")

		# Get entry pointers
		entryAdb = self.builder.get_object("preferencesDialogEntryAdb")
		entrySsh = self.builder.get_object("preferencesDialogEntrySsh")

		# Get entry values
		config.cfg['samcleaner']['adb'] = entryAdb.get_text()
		config.cfg['samcleaner']['ssh'] = entrySsh.get_text()

		# Save preferences
		config.save()

		# ADB status
		thread = threading.Thread(target=self.statusOfAdb)
		thread.start()

		# Hide preferences dialog
		self.preferencesDialog.hide()

	def on_preferencesButton_clicked(self, object, data=None):

		logging.info("signal:preferencesButton:clicked: open preferences dialog")

		self.mainAppMenu.hide()
		self.preferencesDialog.run()

	def on_preferencesDialogButtonCancel_clicked(self, object, data=None):

		logging.info("signal:preferencesDialogButtonCancel:clicked: quit with close button")

		self.preferencesDialog.hide()

	def on_preferencesDialog_response(self, object, data=None):

		logging.info("signal:preferencesDialog:response: quit with escape or system close")

		self.preferencesDialog.hide()

	#
	# Test results dialog
	#
	def on_testDialog_show(self, object, data=None):

		logging.info("signal:testDialog:show: show test results dialog")

	def on_testDialogButtonClose_clicked(self, object, data=None):

		logging.info("signal:testDialogButtonClose:clicked: quit with close button")

		self.testDialog.hide()

	def on_testDialog_response(self, object, data=None):

		logging.info("signal:testDialog:response: quit with escape or system close")

		self.testDialog.hide()

	#
	# About dialog
	#
	def on_aboutButton_clicked(self, object, data=None):

		logging.info("signal:aboutButton:clicked: open about SamCleaner")

		self.mainAppMenu.hide()
		self.aboutAppDialog.run()

	def on_aboutAppDialog_response(self, object, data=None):

		logging.info("signal:aboutAppDialog:response: quit with escape or system close")

		self.aboutAppDialog.hide()

	#
	# Main window
	#
	def on_mainAppWindow_show(self, object, data=None):

		logging.info("signal:mainAppWindow:show: show main app")

		# ADB status
		thread = threading.Thread(target=self.statusOfAdb)
		thread.start()

	def on_mainAppWindow_destroy(self, object, data=None):

		logging.info("signal:mainAppWindow:destroy: quit with close button X")

		Gtk.main_quit()

	def on_gtk_quit_activate(self, menuitem, data=None):

		logging.info("signal:gtk:quit_activate: quit from menu")

		Gtk.main_quit()

	def on_deviceSelector_changed(self, object, data=None):

		# Get selected device ID
		currentDeviceID = self.deviceSelector.get_active_id()

		# Return early if a device has not been selected
		if currentDeviceID==None:
			return False

		logging.info("signal:deviceSelector:changed: device selector changed")

		# Hide phone status image
		self.phoneImageStatus.set_visible(False)

		# Start spinner
		self.phoneSpinnerStatus.start()

		# Disable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, False)

		# Disconnect view from model
		self.adbPackageView.set_model(None)

		# Clear store
		self.adbPackageStore.clear()

		# Update the current device selection to runtime preferences
		config.runtime['samcleaner']['currentDeviceID'] = currentDeviceID

		# Get device packages
		adbPackages = adb.getPackages(currentDeviceID)

		# Loop packages
		for i in range(len(adbPackages)):

			# Replace null string with the empty string
			if adbPackages[i][2]=="null":
				adbPackages[i][2] = ""

			# Convert UID to int
			adbPackages[i][3] = int(adbPackages[i][3])

			# Convert Status to boolean
			if adbPackages[i][4]=="disabled":
				adbPackages[i][4] = False
			else:
				adbPackages[i][4] = True

			# Found flag
			apkNameFound = False

			# Loop APK lists
			for key in self.apkLists:

				# Loop list
				for key2 in self.apkLists[key]:

					if key2!=adbPackages[i][1]:
						continue

					# Package name
					adbPackages[i].append(self.apkLists[key][key2][0])

					# Found flag
					apkNameFound = True

					break

				if apkNameFound==True:

					# Break if a name was found
					break

			# Check if a package name was not found in any of the APK lists
			if apkNameFound==False:

				if adbPackages[i][1] in self.blackList['malware']['ids']:

					# Malware package
					adbPackages[i].append("Malware")

				else:

					# Unknown package
					adbPackages[i].append("")

			# Add package to store
			self.adbPackageStore.append(adbPackages[i])

		# Connect view to model
		self.adbPackageView.set_model(self.treeMenuSort)

		# Enable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, True)

		# Enable the device selector combobox
		GLib.idle_add(self.deviceSelector.set_sensitive, True)

		# Stop spinner
		self.phoneSpinnerStatus.stop()

		if len(adbPackages)>0:

			# Enable the apply button
			GLib.idle_add(self.garbageCleanerButton.set_sensitive, True)

			# Enable the test button
			GLib.idle_add(self.garbageTestButton.set_sensitive, True)

			self.phoneImageStatus.set_from_icon_name("gtk-yes", Gtk.IconSize.LARGE_TOOLBAR)
			self.phoneImageStatus.set_tooltip_text("Scanned %d packages" % len(adbPackages))

		else:

			# Disable the apply button
			GLib.idle_add(self.garbageCleanerButton.set_sensitive, False)

			# Disable the test button
			GLib.idle_add(self.garbageTestButton.set_sensitive, False)

			self.phoneImageStatus.set_from_icon_name("gtk-no", Gtk.IconSize.LARGE_TOOLBAR)
			self.phoneImageStatus.set_tooltip_text("Could not scan for packages")

		# Show phone status image
		self.phoneImageStatus.set_visible(True)

	def on_treeStatusRender_toggled(self, object, path):

		logging.info("signal:treeStatusRender:toggled: status toggle")

		# Get child iterator (iterator of Filter model, which is a child of the Sort model)
		childIter = self.treeMenuFilter.convert_iter_to_child_iter(object.convert_iter_to_child_iter(object[path].iter))

		# Toggle checkbox
		self.adbPackageStore.set_value(childIter, 4, not self.adbPackageStore.get_value(childIter, 4))

		# Hide phone status image
		self.phoneImageStatus.set_visible(False)

		# Start spinner
		self.phoneSpinnerStatus.start()

		# Disable the package view
		GLib.idle_add(self.adbPackageView.set_sensitive, False)

		# ADB toggle
		thread = threading.Thread(target=self.toggleAdbPackage, args=[childIter])
		thread.start()

	def on_refreshButton_clicked(self, object, data=None):

		logging.info("signal:refreshButton:clicked: refresh")

		# ADB status
		thread = threading.Thread(target=self.statusOfAdb)
		thread.start()

	def on_garbageTestButton_clicked(self, object, data=None):

		logging.info("signal:garbageTestButton:clicked: test the garbage")

		# Clear the test view buffer
		self.testDialogResultsViewBuffer.set_text("")

		# Enable the stop button
		self.garbageStopButton.set_sensitive(True)

		# ADB clean
		thread = threading.Thread(target=self.adbSamGarbageTester)
		thread.start()

		# Open the test results view
		self.testDialog.run()

	def on_garbageCleanerButton_clicked(self, object, data=None):

		logging.info("signal:garbageCleanerButton:clicked: clean the garbage")

		# Enable the stop button
		self.garbageStopButton.set_sensitive(True)

		# ADB clean
		thread = threading.Thread(target=self.adbSamGarbageCleaner)
		thread.start()

	def on_garbageStopButton_clicked(self, object, data=None):

		logging.info("signal:garbageStopButton:clicked: stop the clean procedure")

		# Set the stop event
		self.stopThreadEvent.set()

	def on_searchButton_clicked(self, object, data=None):

		logging.info("signal:searchButton:clicked: open search bar")

		# Toggle search bar
		self.searchBar.set_search_mode( not self.searchBar.get_search_mode() )

	def on_searchEntry_search_changed(self, search_entry):

		logging.info("signal:searchEntry:search_changed: search")

		# Search string changed, refilter
		self.treeMenuFilter.refilter()

	def adbPackageView_filter(self, model, iter, search_entry):
		"""Row filter, perform search by package ID"""

		# Get search string from entry buffer
		needle = search_entry.get_buffer().get_text()

		# Get a combo of the package ID and package name
		heystack = model[iter][1]+" "+model[iter][5]

		# Regular expression search (case insensitive)
		if re.search(needle, heystack, re.IGNORECASE)!=None:

			return True

		else:

			return False

	def on_adbPackageView_popup_menu(self, object, data=None):

		logging.info("signal:adbPackageView:popup_menu: popup menu requested")

	def on_adbPackageView_button_press_event(self, object, event):

		logging.info("signal:adbPackageView:button_press_event: button press for package view")

	def on_adbPackageView_button_release_event(self, object, event):

		logging.info("signal:adbPackageView:button_press_event: button release for package view")

	#
	# Signal handler
	#
	def on_signal_handler(self, signum, frame):

		logging.info("signal:mainAppWindow: Caught signal: %d" % signum)

		Gtk.main_quit()

	#
	# Class initialization
	#
	def __init__(self):

		logging.info("mainAppWindow:init: initialize")

		# Catch signals
		signal.signal(signal.SIGHUP, self.on_signal_handler)
		signal.signal(signal.SIGINT, self.on_signal_handler)
		signal.signal(signal.SIGTERM, self.on_signal_handler)

		# Thread stop event
		self.stopThreadEvent = threading.Event()

		# File checks
		if									\
		os.path.isfile("samcleaner.py")==False or				\
		os.path.isfile("assets/G950F_P.txt")==False or				\
		os.path.isfile("assets/G955F_P.txt")==False or				\
		os.path.isfile("assets/G960F_P.txt")==False or				\
		os.path.isfile("assets/G965F_P.txt")==False or				\
		os.path.isfile("assets/G973F_P.txt")==False or				\
		os.path.isfile("assets/G975F_P.txt")==False or				\
		os.path.isfile("assets/G977P_P.txt")==False or				\
		os.path.isfile("assets/N960F_P.txt")==False or				\
		os.path.isfile("assets/G980F_Q.txt")==False or				\
		os.path.isfile("assets/S901U1_S.txt")==False or				\
		os.path.isfile("assets/blacklist.json")==False or			\
		os.path.isfile("assets/samcleaner.glade")==False or			\
		os.path.isfile("assets/samcleaner-icon.png")==False or			\
		os.path.isfile("assets/samcleaner-logo.svg")==False or			\
		os.path.isfile("assets/samcleaner-logo-small.svg")==False or		\
		os.path.isfile("assets/samcleaner-logo-small-squared.svg")==False or	\
		os.path.isfile("assets/style.css")==False:

			# Fail
			logging.error("mainAppWindow:init: required file not found")
			exit()

		# APK lists
		self.apkLists = {}

		# Load package list assets
		self.apkLists['CORRE'] = apklist.load("assets/corrections.txt")
		self.apkLists['G950F'] = apklist.load("assets/G950F_P.txt")
		self.apkLists['G955F'] = apklist.load("assets/G955F_P.txt")
		self.apkLists['G960F'] = apklist.load("assets/G960F_P.txt")
		self.apkLists['G965F'] = apklist.load("assets/G965F_P.txt")
		self.apkLists['G973F'] = apklist.load("assets/G973F_P.txt")
		self.apkLists['G975F'] = apklist.load("assets/G975F_P.txt")
		self.apkLists['G977P'] = apklist.load("assets/G977P_P.txt")
		self.apkLists['N960F'] = apklist.load("assets/N960F_P.txt")
		self.apkLists['G980F'] = apklist.load("assets/G980F_Q.txt")
		self.apkLists['S901U'] = apklist.load("assets/S901U1_S.txt")

		# Blacklist
		try:

			# Load JSON
			self.blackList = json.loads(open('assets/blacklist.json').read())

		except:

			# Fail
			logging.error("mainAppWindow:init: error while loading the blacklist file")
			exit()

		# Load glade UI file
		self.gladefile = "assets/samcleaner.glade"
		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.gladefile)
		self.builder.connect_signals(self)

		# Preferences dialog
		self.preferencesDialog = self.builder.get_object("preferencesDialog")

		# Test results dialog
		self.testDialog = self.builder.get_object("testDialog")
		self.testDialogResultsView = self.builder.get_object("testDialogResultsView")
		self.testDialogResultsViewBuffer = self.testDialogResultsView.get_buffer()

		# About dialog
		self.aboutAppDialog = self.builder.get_object("aboutAppDialog")

		# Main application window
		self.mainAppWindow = self.builder.get_object("mainAppWindow")
		self.mainAppMenu = self.builder.get_object("mainAppMenu")
		self.searchBar = self.builder.get_object("searchBar")
		self.refreshButton = self.builder.get_object("refreshButton")
		self.preferencesButton = self.builder.get_object("preferencesButton")
		self.deviceSelector = self.builder.get_object("deviceSelector")
		self.garbageCleanerButton = self.builder.get_object("garbageCleanerButton")
		self.garbageStopButton = self.builder.get_object("garbageStopButton")
		self.garbageTestButton = self.builder.get_object("garbageTestButton")
		self.informationButton = self.builder.get_object("informationButton")
		self.searchButton = self.builder.get_object("searchButton")
		self.adbImageStatus = self.builder.get_object("adbImageStatus")
		self.phoneImageStatus = self.builder.get_object("phoneImageStatus")
		self.phoneSpinnerStatus = self.builder.get_object("phoneSpinnerStatus")
		self.adbBlacklistStore = self.builder.get_object("adbBlacklistStore")
		self.adbBlacklistView = self.builder.get_object("adbBlacklistView")
		self.adbPackageStore = self.builder.get_object("adbPackageStore")
		self.treeMenuSort = self.builder.get_object("treeMenuSort")
		self.treeMenuFilter = self.builder.get_object("treeMenuFilter")
		self.searchEntry = self.builder.get_object("searchEntry")
		self.adbPackageView = self.builder.get_object("adbPackageView")

		# Set row filter function
		self.treeMenuFilter.set_visible_func(self.adbPackageView_filter, self.searchEntry)

		# Loop blacklist
		for blKey in self.blackList:

			# Add group to store (Name+Action, empty id, Action)
			rowID = self.adbBlacklistStore.append(None,							\
					[										\
						self.blackList[blKey]['name']+" ("+self.blackList[blKey]['action']+")", \
						"",									\
						self.blackList[blKey]['action']						\
					]										\
				)

			# Loop group IDS
			for i in range(len(self.blackList[blKey]['ids'])):

				# Found flag
				apkNameFound = False

				# Blacklist package name - empty by default
				blName = ""

				# Loop APK lists
				for key in self.apkLists:

					# Loop list
					for key2 in self.apkLists[key]:

						if key2!=self.blackList[blKey]['ids'][i]:
							continue

						# Package name
						blName = self.apkLists[key][key2][0]

						# Found flag
						apkNameFound = True

						break

					if apkNameFound==True:

						# Break if a name was found
						break

				# Add ID to store (Name, ID, Action)
				self.adbBlacklistStore.append(rowID,  [ blName, self.blackList[blKey]['ids'][i], self.blackList[blKey]['action'] ] )

		# Expand all blacklist groups
		self.adbBlacklistView.expand_all()

		# CSS style (disabled)
		'''
		from gi.repository import Gdk
		css_provider = Gtk.CssProvider()
		css_provider.load_from_path("assets/style.css")
		context = Gtk.StyleContext()
		screen = Gdk.Screen.get_default()
		context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
		'''

		# Show application
		self.mainAppWindow.show()

if __name__ == "__main__":
	main = SamCleaner()
	Gtk.main()

