"""
SamCleaner ADB run functions

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import re, logging, subprocess, shlex, config

def run(adbParam):
	"""Execute adb and return the output"""

	# Adb binary executable
	adbBin = config.cfg['samcleaner']['adb']

	# SSH command
	if config.cfg['samcleaner']['ssh']=="":
		ssh = ""
	else:
		ssh = config.cfg['samcleaner']['ssh']+" "

	# Select device by serial
	if config.runtime['samcleaner']['currentDeviceID']=="":
		dev = ""
	else:
		dev = " -s "+config.runtime['samcleaner']['currentDeviceID']

	# Adb command
	adbCmd = ssh+adbBin+dev+" "+adbParam

	logging.info("adb run command: %s" % adbCmd)

	# Convert to list
	adbCmd = shlex.split(adbCmd)

	# Run adb
	try:
		result = subprocess.run(adbCmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		return result.stdout.decode('utf-8')
	except:
		return ""

