"""
SamCleaner ADB get functions

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import re, logging, subprocess, config
from adb.run import *

def getVersion():
	"""Get adb version and verify adb is in working order, return the version number."""

	'''
	general commands:
	 version                  show version num
	'''
	# Execute adb
	adbVersion = run("version")

	logging.info(adbVersion)

	# Regular expression match
	match = re.search("\nVersion (.*)\n", adbVersion)

	if match==None:

		# Match not found or adb error
		logging.error("adb:getVersion: adb error")
		return ""

	else:

		# Return matched verion number
		return match.group(1)

def getDevices():
	"""Get adb connected devices.

	Returns a list of devices, each device is a list in the format:
	[ device serial, device model ]
	"""

	'''
	general commands:
	 devices [-l]             list connected devices (-l for long output)
	'''
	# Execute adb
	adbDevices = run("devices -l")

	logging.info(adbDevices)

	# Split by newlines
	adbDevices = re.split("\n+", adbDevices)

	# Remove first element (the header)
	del adbDevices[0]

	# Remove empty items (empty lines)
	adbDevices = [x for x in adbDevices if x.strip()]

	if len(adbDevices)==0 or len(adbDevices[0])<2:

		# No devices found
		logging.warning("adb:getDevices: no devices found")
		return []

	# Split items into separate sublists
	adbDevices = [re.split("\s", entry, 1) for entry in adbDevices]

	# Loop
	for i in range(len(adbDevices)):

		# Early detect devices with no permissions
		if "no permissions;" in adbDevices[i][1]:

			# No permissions
			adbDevices[i] = ""

			continue

		# Split the long device description into a list
		adbDevices[i][1] = re.split(" ", adbDevices[i][1])

		try:

			# Remove empty items (empty lines)
			adbDevices[i][1] = [x for x in adbDevices[i][1] if x]

			# Find the device model
			adbDevices[i][1] = [s for s in adbDevices[i][1] if "model:" in s][0]

			# Remove the prefix
			adbDevices[i][1] = re.sub("^model:", "", adbDevices[i][1]).replace("_", "-")

		except:

			# No model code, may happen with rooted devices, use serial ID
			adbDevices[i][1] = adbDevices[i][0]

	# Remove empty items (empty lines)
	adbDevices = [x for x in adbDevices if x]

	if len(adbDevices)==0:

		# No devices found
		logging.warning("adb:getDevices: no devices found")
		return []

	logging.info(adbDevices)

	return adbDevices

def detectPackageManager():
	"""Detect the device package manager. For ancient android the 'pm'
	command will be used, while on modern android the 'cmd' command will
	be the default.
	"""

	# Run a test command with cmd
	rc = run("shell cmd package")

	# Check for ancient android
	if " cmd: not found" in rc:

		# Change to pm commands
		config.runtime['samcleaner']['command'] = "pm"

	else:

		# Change to cmd commands
		config.runtime['samcleaner']['command'] = "cmd"

	logging.info("adb:detectPackageManager: detected the '%s' device manager", config.runtime['samcleaner']['command'])

def getPackages(currentDeviceID):
	"""Get all packages.

	Returns a list of packages, each package is a list in the format:
	[APK, ID, Install, UID, Status]

	The name of the package is empty at this stage and requires matching
	with the apklist module.
	"""

	# Validate device selection
	if currentDeviceID==None:

		# No selection
		return []

	# Detect the device package manager
	detectPackageManager()

	# Validate package manager
	if config.runtime['samcleaner']['command']=="":

		# No package manager
		return []

	'''
	  list packages [-f] [-d] [-e] [-s] [-3] [-i] [-l] [-u] [-U] 
	      [--uid UID] [--user USER_ID] [FILTER]
	    Prints all packages; optionally only those whose name contains
	    the text in FILTER.  Options are:
	      -f: see their associated file
	      -d: filter to only show disabled packages
	      -e: filter to only show enabled packages
	      -s: filter to only show system packages
	      -3: filter to only show third party packages
	      -i: see the installer for the packages
	      -l: ignored (used for compatibility with older releases)
	      -U: also show the package UID
	      -u: also include uninstalled packages
	      --uid UID: filter to only show packages with the given UID
	      --user USER_ID: only list packages belonging to the given user
	'''
	if config.runtime['samcleaner']['command']=="cmd":

		# Execute adb (enabled packages only)
		adbPackages = run("shell cmd package list packages -f -i -U --user 0 -e")

		# Perform newline conversion
		adbPackages = re.sub(r"\r\x0A", "\n", adbPackages, 0, re.MULTILINE)

	elif config.runtime['samcleaner']['command']=="pm":

		# Execute adb (enabled packages only)
		adbPackages = run("shell pm list packages -f -i --user 0 -e")

		# Add a missing UID and perform newline conversion
		adbPackages = re.sub(r"\r\x0A", " uid:0\n", adbPackages, 0, re.MULTILINE)

	# Check for valid input (barely...)
	if not "com.android." in adbPackages:
		return []

	# Remove various strings to simplify string separation, add enabled status
	adbPackages = adbPackages.replace("package:", "").replace("installer=", "").replace("uid:", "").replace(".apk=", ".apk ").replace("  ", " ").replace("\n", " enabled\n")

	if config.runtime['samcleaner']['command']=="cmd":

		# Execute adb (disabled packages only)
		adbPackagesDisabled = run("shell cmd package list packages -f -i -U --user 0 -d")

		# Perform newline conversion
		adbPackagesDisabled = re.sub(r"\r\x0A", "\n", adbPackagesDisabled, 0, re.MULTILINE)

	elif config.runtime['samcleaner']['command']=="pm":

		# Execute adb (disabled packages only)
		adbPackagesDisabled = run("shell pm list packages -f -i --user 0 -d")

		# Add a missing UID and perform newline conversion
		adbPackagesDisabled = re.sub(r"\r\x0A", " uid:0\n", adbPackagesDisabled, 0, re.MULTILINE)

	# Remove various strings to simplify string separation, add disabled status
	adbPackagesDisabled = adbPackagesDisabled.replace("package:", "").replace("installer=", "").replace("uid:", "").replace(".apk=", ".apk ").replace("  ", " ").replace("\n", " disabled\n")

	# Combine the two package lists into one
	adbPackages = adbPackages+"\n"+adbPackagesDisabled

	# Split by newlines
	adbPackages = re.split("\n+", adbPackages)

	# Remove empty items (empty lines)
	adbPackages = [x for x in adbPackages if x.strip()]

	# Split items into separate sublists
	adbPackages = [re.split(" ", entry) for entry in adbPackages]

	logging.info("adb:getPackages: detected %d packages", len(adbPackages))

	return adbPackages

