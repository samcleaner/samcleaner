"""
SamCleaner ADB package functions

Copyright (C) 2019

This file is part of SamCleaner.

SamCleaner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SamCleaner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SamCleaner.  If not, see <http://www.gnu.org/licenses/>.
"""

# Import required modules
import re, logging, subprocess, config
from adb.run import *

def packageEnable(packageId):
	"""Enable package via adb shell command."""

	# Validate package manager
	if config.runtime['samcleaner']['command']=="":

		# No package manager
		return False

	'''
	  enable [--user USER_ID] PACKAGE_OR_COMPONENT
	  default-state [--user USER_ID] PACKAGE_OR_COMPONENT
	    These commands change the enabled state of a given package or
	    component (written as "package/class").
	'''
	if config.runtime['samcleaner']['command']=="cmd":

		# Execute adb
		adbRc = run("shell cmd package enable %s" % packageId)

	elif config.runtime['samcleaner']['command']=="pm":

		# Execute adb
		adbRc = run("shell pm enable %s" % packageId)

	logging.info(adbRc)

	# Validate rc
	if " new state: enabled" in adbRc:
		return True
	else:
		return False

def packageDisable(packageId):
	"""Disable package via adb shell command."""

	# Validate package manager
	if config.runtime['samcleaner']['command']=="":

		# No package manager
		return False

	'''
	  disable [--user USER_ID] PACKAGE_OR_COMPONENT
	  disable-user [--user USER_ID] PACKAGE_OR_COMPONENT
	  disable-until-used [--user USER_ID] PACKAGE_OR_COMPONENT
	  default-state [--user USER_ID] PACKAGE_OR_COMPONENT
	    These commands change the enabled state of a given package or
	    component (written as "package/class").
	'''
	if config.runtime['samcleaner']['command']=="cmd":

		# Execute adb
		adbRc = run("shell cmd package disable-user %s" % packageId)

	elif config.runtime['samcleaner']['command']=="pm":

		# Execute adb
		adbRc = run("shell pm disable-user %s" % packageId)

	logging.info(adbRc)

	'''
	  force-stop [--user <USER_ID> | all | current] <PACKAGE>
	      Completely stop the given application package.
	'''
	# Force stop and ignore the result
	adbRcStop = run("shell am force-stop --user 0 %s" % packageId)
	logging.info(adbRcStop)

	# Validate rc
	if " new state: disabled-user" in adbRc:
		return True
	else:
		return False

def packageClear(packageId):
	"""Clear package data via adb shell command."""

	# Validate package manager
	if config.runtime['samcleaner']['command']=="":

		# No package manager
		return False

	'''
	  clear [--user USER_ID] PACKAGE
	    Deletes all data associated with a package.
	'''
	if config.runtime['samcleaner']['command']=="cmd":

		# Execute adb
		run("shell cmd package clear %s" % packageId)
		run("shell cmd package clear --user 0 %s" % packageId)

	elif config.runtime['samcleaner']['command']=="pm":

		# Execute adb
		run("shell pm clear %s" % packageId)
		run("shell pm clear --user 0 %s" % packageId)

	return True

def packageUninstall(packageId):
	"""Uninstall package via adb shell command."""

	# Validate package manager
	if config.runtime['samcleaner']['command']=="":

		# No package manager
		return False

	# Disable as a first step and ignore the result
	packageDisable(packageId)

	# Clear package data
	packageClear(packageId)

	'''
	  pm uninstall [-k] [--user USER_ID] [--versionCode VERSION_CODE] PACKAGE [SPLIT]
	    Remove the given package name from the system.  May remove an entire app
	    if no SPLIT name is specified, otherwise will remove only the split of the
	    given app.  Options are:
	      -k: keep the data and cache directories around after package removal.
	      --user: remove the app from the given user.
	      --versionCode: only uninstall if the app has the given version code.
	'''
	if config.runtime['samcleaner']['command']=="cmd":

		# Execute adb
		adbRc = run("shell cmd package uninstall --user 0 %s" % packageId)

	elif config.runtime['samcleaner']['command']=="pm":

		# Execute adb
		adbRc = run("shell pm uninstall %s" % packageId)

	logging.info(adbRc)

